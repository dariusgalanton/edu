#!/bin/bash

#script that changes the current directory with a given one and immediately lists the dir
#has to be called with "source"
#made out of necessity;everytime you change the current directory most probably you need to list it,so why not do it with one command?

function cdir(){
	cd $1
	ls 
}

if test $# -eq 0;then
	echo Please specify the directory
	exit 1
fi

cdir $1

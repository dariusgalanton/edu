#!/bin/bash

#script that prints all the class definitions from a given file

if test $# -eq 0;then
	echo You need to specify at least one file
	exit 1
fi

for file in $*;do
	cat $file | grep "class" | awk -F" " '{print $2}' | sort | uniq
done


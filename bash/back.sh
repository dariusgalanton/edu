#!/bin/bash

#bash script that changes the current directory with the parent one as many times as given
#script has to be called with "source"

if test $# -ne 1 ;then
	echo You need to specify the number of directories for back
	return 1
fi

NR=$1
DIR=$PWD
for i in `seq 1 $NR`;do
	DIR=$DIR/..
done
cd $DIR

#!/bin/bash

#script that makes a backup of the given files in a certain backup path

FILE_NAME=`basename "$0"`
BACKUP_PATH=/home/backup

if test $# -eq 0;then
	echo You need to specify the option and at least one file
	exit 1
fi

OPT_ARG=$1
shift

if test $OPT_ARG = "-b";then
	for FILE in $*;do
		cp $FILE $BACKUP_PATH/$FILE
	done
	echo Files backed up succesfully.
	exit 0
fi

if test $OPT_ARG = "-h";then
	echo This is a script that makes a backup of the given files in a certain backup path
	echo Optional arguments :
	echo -h - displays this help and exits
	echo -b "[FILE]" - makes a backup of the given files
	echo -path "[PATH]"- changes the backup path with a given one
	exit 0
fi

if test $OPT_ARG = "-path";then
	sed -i "s+^BACKUP_PATH.*+BACKUP_PATH=$1+" "$FILE_NAME"
	echo Backup path modified succesfully.
	exit 0
fi

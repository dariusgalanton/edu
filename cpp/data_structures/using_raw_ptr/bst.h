#pragma once

#include <string>
#include <assert.h>
#include <functional>
#include <algorithm>
#include "../utils/utils.h"
#include "../../data_structures/using_smart_ptr/dynamicarray.h"

class BinarySearchTreeException {
private:
	std::string message;
public:
	BinarySearchTreeException(std::string msg) : message{ msg } {}

	friend std::ostream& operator<<(std::ostream& out, const BinarySearchTreeException& err) {
		out << err.message;
		return out;
	}
};

template<typename ElementType>
struct node {
	ElementType element;
	node<ElementType>* left, *right;

	node(ElementType elem) : element{ elem }, left{ nullptr }, right{ nullptr } {}
};

template<typename ElementType>
class BinarySearchTree {
protected:
	node<ElementType>* root;
	
	//the given function for comparison should return -1,0 or 1 depending on comparisons between elements
	std::function<int(ElementType, ElementType)> cmp_function;

	//creates a node with the given element and returns it
	node<ElementType>* createNode(const ElementType& element) {
		node<ElementType>* newNode = new node<ElementType>{ element };
		return newNode;
	}

	//adds the given element in the tree
	//param : element - ElementType
	//param : currentNode - node<ElementType>*
	//throws BinarySearchTreeException if the element already exists
	node<ElementType>* add(const ElementType& element, node<ElementType>* currentNode, node<ElementType>* parent = nullptr) {
		if (currentNode == nullptr) {
			currentNode = createNode(element);
			
			if (parent != nullptr)
				if (this->cmp_function(currentNode->element, parent->element) > 0) {
					parent->right = currentNode;
				} else parent->left = currentNode;
		} else if (this->cmp_function(element, currentNode->element) < 0) {
			this->add(element, currentNode->left, currentNode);
		} else if (this->cmp_function(element, currentNode->element) > 0) {
			this->add(element, currentNode->right, currentNode);
		} else if (!this->cmp_function(element, currentNode->element)) {
			throw BinarySearchTreeException("Element already exists in BST!");
		}
		
		return currentNode;
	}

	//returns a pointer to the node with the element searched
	//param : element - ElementType
	//param : currentNode - node<ElementType>*
	//throws BinarySearchException if the element is not found
	node<ElementType>* search(const ElementType& element, node<ElementType>* currentNode) {
		if (currentNode == nullptr) {
			throw BinarySearchTreeException("Element does not exist in BST!");
		}

		if (!this->cmp_function(currentNode->element, element)) {
			return currentNode;
		} else if (this->cmp_function(element, currentNode->element) < 0) {
			this->search(element, currentNode->left);
		} else this->search(element, currentNode->right);
	}

	//traverses the tree in preorder and saves the elements in a given array
	//param : arr - DynamicArray
	//currentNode : node*
	void pre_order(DynamicArray<ElementType>& arr, node<ElementType>* currentNode) {
		if (currentNode == nullptr) {
			return;
		}

		arr.push_back(currentNode->element);
		this->pre_order(arr, currentNode->left);
		this->pre_order(arr, currentNode->right);
	}

	//traverses the tree in inorder and saves the elements in a given array
	//param : arr - DynamicArray
	//currentNode : node*
	void in_order(DynamicArray<ElementType>& arr, node<ElementType>* currentNode) {
		if (currentNode == nullptr) {
			return;
		}

		this->in_order(arr, currentNode->left);
		arr.push_back(currentNode->element);
		this->in_order(arr, currentNode->right);
	}

	//traverses the tree in postorder and saves the elements in a given array
	//param : arr - DynamicArray
	//currentNode : node*
	void post_order(DynamicArray<ElementType>& arr, node<ElementType>* currentNode) {
		if (currentNode == nullptr) {
			return;
		}

		this->post_order(arr, currentNode->left);
		this->post_order(arr, currentNode->right);
		arr.push_back(currentNode->element);
	}

	//deallocates the subtree with the given root
	void destroy(node<ElementType>* currentNode) {
		if (currentNode == nullptr) {
			return;
		}

		this->destroy(currentNode->left);
		this->destroy(currentNode->right);
		delete currentNode;
	}

	//returns the adress of the node with the minimum from a given subtree
	node<ElementType>* minimum(node<ElementType>* startNode) {
		node<ElementType>* currentNode = startNode;

		while (currentNode->left != nullptr) {
			currentNode = currentNode->left;
		}

		return currentNode;
	}

	//erases the given element from the tree and returns the node of the new tree
	//param : element - ElementType
	//param : currentNode - node*
	//param : parent - node*
	node<ElementType>* erase(const ElementType& element, node<ElementType>* currentNode) {
		if (currentNode == nullptr) {
			throw BinarySearchTreeException("Element does not exist in BST!");
		} else {
			if(this->cmp_function(element,currentNode->element) < 0) {
				currentNode->left = this->erase(element, currentNode->left);
				return currentNode;
			} else if (this->cmp_function(element,currentNode->element) > 0){
				currentNode->right = this->erase(element, currentNode->right);
				return currentNode;
			} else {
				if (currentNode->left != nullptr && currentNode->right != nullptr) {
					node<ElementType>* aux = this->minimum(currentNode->right);
					currentNode->element = aux->element;
					currentNode->right = this->erase(currentNode->element, currentNode->right);
					
					return currentNode;
				} else {
					node<ElementType>* aux;
					
					if (currentNode->left == nullptr) {
						aux = currentNode->right;
					}

					else aux = currentNode->left;
					delete currentNode;
					
					return aux;
				}
			}
		}
	}

public:
	//constructor
	BinarySearchTree(std::function<int(ElementType, ElementType)> func) : root{ nullptr }, cmp_function{func} {}

	//copy constructor
	BinarySearchTree(const BinarySearchTree& other) {
		this->root = nullptr;
		this->cmp_function = other.cmp_function;
		DynamicArray<ElementType> arr;
		node<ElementType>* aux = other.root;
		this->pre_order(arr, aux);

		for (auto elem : arr) {
			this->public_add(elem);
		}
	}

	//op= overload
	BinarySearchTree<ElementType>& operator=(const BinarySearchTree& other) {
		this->destroy(this->root);
		DynamicArray<ElementType> arr;
		node<ElementType>* aux = other.root;
		this->pre_order(arr, aux);

		for (auto elem : arr) {
			this->public_add(elem);
		}

		return *this;
	}

	//public add so that we can hide the add call with the root
	virtual void public_add(const ElementType& element) {
		this->root = this->add(element, this->root);
	}

	//public search
	node<ElementType>* public_search(const ElementType& element) {
		return this->search(element, this->root);
	}

	//returns true if the tree is empty;false otherwise
	bool empty() {
		return this->root == nullptr;
	}

	//returns an array with the elements in preorder
	DynamicArray<ElementType> preorder_traversal() {
		DynamicArray<ElementType> arr;
		this->pre_order(arr, this->root);
		return arr;
	}

	//returns an array with the elements in inorder
	DynamicArray<ElementType> inorder_traversal() {
		DynamicArray<ElementType> arr;
		this->in_order(arr, this->root);
		return arr;
	}

	//returns an array with the elements in postorder
	DynamicArray<ElementType> postorder_traversal() {
		DynamicArray<ElementType> arr;
		this->post_order(arr, this->root);
		return arr;
	}

	//public erase
	virtual void public_erase(const ElementType& element) {
		if (this->empty()) {
			throw BinarySearchTreeException("The element given does not exist in BST!");
		}

		this->root = this->erase(element, this->root);
	}

	//destructor
	~BinarySearchTree() {
		this->destroy(this->root);
	}

};

template<typename ElementType>
class AVLTree : public BinarySearchTree<ElementType> {
private:
	int height(node<ElementType>* currentNode) {
		if (currentNode == nullptr) {
			return 0;
		}
		
		return 1 + std::max(this->height(currentNode->left), this->height(currentNode->right));
	}

	node<ElementType>* left_rotation(node<ElementType>* currentNode) {
		if (currentNode == nullptr || currentNode->right == nullptr) {
			return currentNode;
		}

		node<ElementType>* right = currentNode->right;
		currentNode->right = right->left;
		right->left = currentNode;

		return right;
	}

	node<ElementType>* right_rotation(node<ElementType>* currentNode) {
		if (currentNode == nullptr || currentNode->left == nullptr) {
			return currentNode;
		}

		node<ElementType>* left = currentNode->left;
		currentNode->left = left->right;
		left->right = currentNode;

		return left;
	}

	node<ElementType>* left_right_rotation(node<ElementType>* currentNode) {
		currentNode->left = this->left_rotation(currentNode->left);
		currentNode = this->right_rotation(currentNode);

		return currentNode;
	}

	node<ElementType>* right_left_rotation(node<ElementType>* currentNode) {
		currentNode->right = this->right_rotation(currentNode->right);
		currentNode = this->left_rotation(currentNode);

		return currentNode;
	}

	node<ElementType>* rebalance(node<ElementType>* currentNode, const ElementType& element) {
		if (this->cmp_function(element, currentNode->element) > 0) {
			if (this->height(currentNode->right) - this->height(currentNode->left) > 1) {
				if (this->cmp_function(element, currentNode->right->element) > 0) {
					currentNode = this->left_rotation(currentNode);
				} else currentNode = this->left_right_rotation(currentNode);
			}
		} else if (this->cmp_function(element, currentNode->element) < 0) {
			if (this->height(currentNode->left) - this->height(currentNode->right) > 1) {
				if (this->cmp_function(element, currentNode->left->element) < 0) {
					currentNode = this->right_rotation(currentNode);
				} else currentNode = this->right_left_rotation(currentNode);
			}
		}

		return currentNode;
	}

	static int cmp(ElementType first, ElementType second) {
		if (first < second) {
			return -1;
		}
		else if (first == second) {
			return 0;
		}

		return 1;
	}

public:
	AVLTree(std::function<int(ElementType, ElementType)> func) : BinarySearchTree(func) {}

	AVLTree(const AVLTree& other) : BinarySearchTree(other) {}

	AVLTree() : BinarySearchTree(AVLTree::cmp) {};

	void public_add(const ElementType& element) override {
		this->root = this->add(element, this->root);
		this->root = this->rebalance(this->root, element);
	}

	void public_erase(const ElementType& element) override {
		this->root = this->erase(element, this->root);
		this->root = this->rebalance(this->root, element);
	}

	~AVLTree() {  }
};

void test_bst() {
	//cmp is a function that returns -1 if the first element is lesser,0 if the elements are equal,1 if the first element is bigger
	BinarySearchTree<int> tree{ Utils::cmp<int> };
	assert(tree.empty() == true);
	tree.public_add(1);
	assert(tree.empty() == false);
	tree.public_add(2);
	tree.public_add(-1);
	tree.public_add(3);
	tree.public_add(0);
	DynamicArray<int> arr = tree.preorder_traversal();
	assert(arr[0] == 1);
	assert(arr[1] == -1);
	assert(arr[2] == 0);
	assert(arr[3] == 2);
	assert(arr[4] == 3);
	BinarySearchTree<int> other{ Utils::cmp<int> };
	other = tree;
	BinarySearchTree<int> oty{ tree };
	DynamicArray<int> otarr = oty.inorder_traversal();
	assert(otarr[0] == -1);
	assert(otarr[1] == 0);
	assert(otarr[2] == 1);
	assert(otarr[3] == 2);
	assert(otarr[4] == 3);
	otarr = other.inorder_traversal();
	assert(otarr[0] == -1);
	assert(otarr[1] == 0);
	assert(otarr[2] == 1);
	assert(otarr[3] == 2);
	assert(otarr[4] == 3);
	arr = tree.postorder_traversal();
	assert(arr[0] == 0);
	assert(arr[1] == -1);
	assert(arr[2] == 3);
	assert(arr[3] == 2);
	assert(arr[4] == 1);

	tree.public_erase(1);
	tree.public_erase(0);
	arr = tree.preorder_traversal();
	assert(arr[0] == 2);
	assert(arr[1] == -1);
	assert(arr[2] == 3);

	//check if after deletion from one tree the modifications don't reflect on the other
	assert(otarr[0] == -1);
	assert(otarr[1] == 0);
	assert(otarr[2] == 1);
	assert(otarr[3] == 2);
	assert(otarr[4] == 3);
	BinarySearchTree<int> btree{ Utils::cmp<int> };
	try {
		btree.public_erase(0);
		assert(false);
	} catch (BinarySearchTreeException) {
		assert(true);
	}

	try {
		tree.public_add(2);
		assert(false);
	} catch (BinarySearchTreeException) {
		assert(true);
	}

	AVLTree<int> a{ Utils::cmp<int> };
	a.public_add(1);
	a.public_add(2);
	a.public_add(3);
	a.public_add(-1);
	a.public_add(-2);

	AVLTree<int> b{ a };
	AVLTree<int> c = a;

	arr = b.inorder_traversal();
	assert(arr[0] == -2);
	assert(arr[1] == -1);
	assert(arr[2] == 1);
	assert(arr[3] == 2);
	assert(arr[4] == 3);

	b.public_erase(2);
	arr = a.inorder_traversal();
	assert(arr[3] == 2);

	arr = c.inorder_traversal();
	assert(arr[0] == -2);
	assert(arr[1] == -1);
	assert(arr[2] == 1);
	assert(arr[3] == 2);
	assert(arr[4] == 3);

	c.public_erase(-1);
	arr = a.inorder_traversal();
	assert(arr[1] == -1);
}


#pragma once
#include<assert.h>
#include<string>

template<typename ElementType>
struct node {
	ElementType element;
	node<ElementType>* next;
	node<ElementType>* prev;

	node(const ElementType& el) : element{ el }, next{ nullptr }, prev{ nullptr } {};
};

class DequeueException {
private :
	std::string message;
public :
	DequeueException(std::string mess) : message{ mess } {};

	friend std::ostream& operator<<(std::ostream& out, const DequeueException& er) {
		out << er;
		return out;
	}
};

template<typename ElementType>
class Dequeue {
	//dequeue implemented on double linked list
private :
	node<ElementType>* head;
	node<ElementType>* tail;
public :
	//constructor
	Dequeue() {
		this->head = nullptr;
		this->tail = nullptr;
	}

	//copy constructor
	Dequeue(const Dequeue& ot) {
		this->head = nullptr;
		this->tail = nullptr;
		node<ElementType>* aux = ot.head;
		
		while (aux != nullptr) {
			this->push_back(aux->element);
			aux = aux->next;
		}
	}

	//op= overload
	Dequeue<ElementType>& operator=(const Dequeue& ot) {
		this->~Dequeue();
		this->head = nullptr;
		this->tail = nullptr;
		node<ElementType>* aux = ot.head;
		
		while (aux != nullptr) {
			this->push_back(aux->element);
			aux = aux->next;
		}
		
		return *this;
	}

	//adds an element in the front of queue
	//param : element - ElementType
	void push_front(const ElementType& element) {
		if (this->head == nullptr) {
			this->head = new node<ElementType>(element);
			this->tail = this->head;
		
			return;
		}
		
		node<ElementType>* newNode = new node<ElementType>(element);
		newNode->next = this->head;
		this->head->prev = newNode;
		this->head = newNode;
	}

	//adds an element in the tail of queue
	//param : element - ElementType
	void push_back(const ElementType& element) {
		if (this->head == nullptr) {
			this->head = new node<ElementType>(element);
			this->tail = this->head;
		
			return;
		}

		node<ElementType>* newNode = new node<ElementType>(element);
		newNode->prev = this->tail;
		this->tail->next = newNode;
		this->tail = newNode;
	}

	//deletes the element from the front of the dequeue and returns it
	//throws DequeException if the deque is empty
	ElementType pop_front() {
		if (this->head == nullptr) {
			throw DequeueException("Deque empty!");
		}

		node<ElementType>* aux = this->head;
		this->head = this->head->next;
		
		if (this->head != nullptr) {
			this->head->prev = nullptr;
		}
		
		ElementType toReturn = aux->element;
		delete aux;
		
		return toReturn;
	}

	//deletes the element form the tail of the deque and returns it
	//throws DequeException if the deque is empty
	ElementType pop_back() {
		if (this->tail == nullptr) {
			throw DequeueException("Dequeue empty!");
		}

		node<ElementType>* aux = this->tail;
		
		if (this->tail->prev == nullptr) {
			this->head = nullptr;
		}
		
		this->tail = this->tail->prev;
		
		if (this->tail != nullptr) {
			this->tail->next = nullptr;
		}
		
		ElementType toReturn = aux->element;
		delete aux;
		
		return toReturn;
	}

	//returns a copy of the element in front of the deque
	ElementType peek_front() {
		return this->head->element;
	}

	//returns a copy of the element in rear of the deque
	ElementType peek_back() {
		return this->tail->element;
	}

	//returns whether the deque is empty or not
	bool isEmpty() {
		return this->head == nullptr || this->tail==nullptr;
	}

	//destructor
	~Dequeue() {
		while (this->head != nullptr){
			node<ElementType>* toDelete = this->head;
			this->head = this->head->next;
			delete toDelete;
		}
	}
};

void test_deque() {
	Dequeue<int> d;
	assert(d.isEmpty());
	d.push_front(1);
	assert(!d.isEmpty());
	assert(d.peek_back() == 1);
	assert(d.peek_front() == 1);
	d.push_front(2);
	assert(d.peek_back() == 1);
	assert(d.peek_front() == 2);
	d.push_front(3);
	assert(d.peek_front() == 3);
	d.push_front(4);
	assert(d.peek_front() == 4);
	d.push_back(5);
	assert(d.peek_back() == 5);
	d.push_back(6);
	assert(d.peek_back() == 6);
	d.push_back(7);
	assert(d.peek_back() == 7);

	Dequeue<int> otherdeq{ d };

	assert(d.pop_back() == 7);
	assert(d.pop_back() == 6);
	assert(d.pop_front() == 4);
	assert(d.pop_front() == 3);
	assert(d.pop_back() == 5);
	assert(d.pop_front() == 2);
	assert(d.pop_back() == 1);
	try {
		d.pop_back();
		assert(false);
	} catch (const DequeueException& er) {
		assert(true);
	}
	try {
		d.pop_front();
		assert(false);
	} catch (const DequeueException& er) {
		assert(true);
	}

	Dequeue<int> alt;
	alt.push_back(1);
	assert(alt.peek_back() == 1);
	assert(alt.peek_front() == 1);

	assert(!otherdeq.isEmpty());
	assert(otherdeq.peek_back() == 7);
	assert(otherdeq.peek_front() == 4);

	Dequeue<int> ot;
	ot = otherdeq;
	assert(ot.peek_back() == 7);
	assert(ot.peek_front() == 4);
	otherdeq.pop_back();
	otherdeq.pop_front();
	assert(ot.peek_back() == 7);
	assert(ot.peek_front() == 4);
}
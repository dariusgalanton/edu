#pragma once
#include<assert.h>
#include<string>

template<typename ElementType>
struct node {
	ElementType element;
	node<ElementType>* next;

	node(const ElementType& elem) : element{ elem }, next{ nullptr } {}
};

class StackException {
private :
	std::string message;
public :
	StackException(const std::string& mess) : message{ mess } {}

	friend std::ostream& operator<<(std::ostream& out, const StackException& err) {
		out << err.message;
		return out;
	}
};

template<typename ElementType>
class Stack {
private :
	node<ElementType>* head;
public :
	//constructor
	Stack() : head{ nullptr } {}

	//copy constructor
	Stack(const Stack& ot) {
		this->head = nullptr;
		Stack<ElementType> aux;
		node<ElementType>* other = ot.head;
		
		while (other != nullptr) {
			aux.push(other->element);
			other = other->next;
		
		}
		
		other = aux.head;
		
		while (other != nullptr) {
			this->push(other->element);
			other = other->next;
		}
	}

	//op= overload
	Stack<ElementType>& operator=(const Stack& ot) {
		this->~Stack();
		Stack<ElementType> aux;
		node<ElementType>* other = ot.head;
		
		while (other != nullptr) {
			aux.push(other->element);
			other = other->next;
		}
		
		other = aux.head;
		
		while (other != nullptr) {
			this->push(other->element);
			other = other->next;
		}
		
		return *this;
	}

	//adds a given element to the stack
	void push(const ElementType& element) {
		node<ElementType>* newHead = new node<ElementType>{element};
		newHead->next = this->head;
		this->head = newHead;
	}

	//deletes the element from the stack and returns it
	ElementType pop() {
		if (this->head != nullptr) {
			node<ElementType>* retNode = this->head;
			ElementType retElement = retNode->element;
			this->head = this->head->next;
			delete retNode;
			return retElement;
		}

		throw StackException("Stack empty!");
	}

	//returns True or False wheter the stack is empty
	bool empty() {
		return this->head == nullptr;
	}

	//returns a copy of the element on the top of the stack
	ElementType access() {
		if (this->empty()) {
			throw StackException("Stack empty!");
		}

		return this->head->element;
	}

	//destructor 
	~Stack() {
		node<ElementType>* toDelete = this->head;
		
		while (this->head != nullptr) {
			this->head = this->head->next;
			delete toDelete;
			toDelete = this->head;
		}
	}
};

void test_stack() {
	Stack<int> st;
	st.push(1);
	assert(st.access() == 1);
	st.push(2);
	assert(st.access() == 2);
	st.push(3);
	assert(st.access() == 3);

	assert(st.empty() == false);
	Stack<int> ot ;
	ot=st;

	Stack<int> other{ ot };
	assert(other.pop() == 3);
	assert(other.pop() == 2);
	assert(other.pop() == 1);
	assert(ot.empty() == false);

	assert(st.pop() == 3);
	assert(st.pop() == 2);
	assert(st.pop() == 1);
	assert(ot.empty() == false);

	try {
		int nr = st.pop();
		assert(false);
	} catch (const StackException& err) {
		assert(true);
	}

	try {
		int nr = st.access();
		assert(false);
	} catch (const StackException& err) {
		assert(true);
	}
	
	assert(st.empty() == true);
}
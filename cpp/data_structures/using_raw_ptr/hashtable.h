#pragma once
#include "../using_smart_ptr/dynamicarray.h"
#include"../using_raw_ptr/bst.h"
#include <string>
#include<string.h>
#include<iostream>

#define DELETED -9999
#define FREE -9998

//hash function
int hash_int(int key,int dimension) {
	return key%dimension;
}

int hash_str(std::string key,int dimension) {
	const char* str = key.c_str();
	int checksum = 0;
	
	for (int i = 0; i < strlen(str); i++) {
		checksum += str[i];
	}
	
	return checksum%dimension;
}

template<typename KeyType,typename ElementType>
struct Element {
	KeyType key;
	ElementType element;

	Element(const KeyType& k, const ElementType& el) : key{ k }, element{ el } {};
	Element(const ElementType& el) : element{ el } {};
	Element() = default;

	bool operator==(const Element& other) {
		return this->element == other.element;
	}
	bool operator!=(const Element& other) {
		return !(*this == other);
	}

	bool operator<(const Element& other) {
		return this->element < other.element;
	}
	
	bool operator>(const Element& other) {
		return this->element > other.element;
	}
};

template<typename KeyType,typename ElementType>
class HashTableInterface {
public :
	virtual void add(const Element<KeyType, ElementType>& element) = 0;
	virtual void erase(const Element<KeyType, ElementType>& element) = 0;
	virtual bool isEmpty() = 0;
	virtual bool search(const Element<KeyType, ElementType>& element) = 0;
	virtual KeyType search(const ElementType& element) = 0;
};

class HashTableException {
private :
	std::string message;
public :
	HashTableException(std::string mess) : message{ mess } {};

	friend std::ostream& operator<<(std::ostream& out, const HashTableException& ex) {
		out << ex;
		return out;
	}
};

template<typename KeyType,typename ElementType>
class OpenAdressingHashTableIterator;

template<typename KeyType,typename ElementType>
class OpenAdressingHashTable : public HashTableInterface<KeyType,ElementType> {
	//linear probing
private :
	int dimension;
	Element<KeyType, ElementType> * elements;
	std::function<int(KeyType, int)> hash_func;

	void rehash() {
		Element<KeyType, ElementType> *newTable = new Element<KeyType, ElementType>[dimension * 2];
		for (int i = 0; i < dimension * 2; i++) {
			newTable[i] = FREE;
		}

		for (int i = 0; i < dimension; i++) {
			int pos = this->hash_func(this->elements[i].key, dimension * 2);

			while (newTable[pos] != FREE && pos < dimension * 2){
				pos++;
			}

			newTable[pos] = this->elements[i];
		}

		delete this->elements;
		this->dimension = this->dimension * 2;
		this->elements = newTable;
	}

public :
	OpenAdressingHashTable(int dim, std::function<int(KeyType, int)> hash) : dimension{ dim }, hash_func{ hash } {
		elements = new Element<KeyType, ElementType>[dimension];

		for (int i = 0; i < dimension; i++){
			this->elements[i] = FREE;
		}
	}

	//Rule of three ensured
	OpenAdressingHashTable(const OpenAdressingHashTable& other) {
		this->elements = new Element<KeyType, ElementType>[other.dimension];
		
		for (int i = 0; i < other.dimension; i++) {
			this->elements[i] = other.elements[i];
		}
		
		this->dimension = other.dimension;
		this->hash_func = other.hash_func;
	}

	OpenAdressingHashTable<KeyType, ElementType>& operator=(const OpenAdressingHashTable& other) {
		delete this->elements;
		this->elements = new Element<KeyType, ElementType>[other.dimension];

		for (int i = 0; i < other.dimension; i++) {
			this->elements[i] = other.elements[i];
		}
		this->dimension = other.dimension;
		this->hash_func = other.hash_func;
		
		return *this;
	}

	void add(const Element<KeyType, ElementType>& element) override {
		int pos = this->hash_func(element.key,dimension);

		while (this->elements[pos] != FREE && this->elements[pos] != DELETED && pos < dimension) {
			if (this->elements[pos] == element) {
				throw HashTableException("Element already exists in HT!");
			}

			pos++;
		}
		
		if (pos == dimension) {
			//RESIZE AND REHASH!
			this->rehash();
		}

		this->elements[pos] = element;
	}

	void erase(const Element<KeyType, ElementType>& element) override {
		int pos = this->hash_func(element.key, dimension);
		
		while (this->elements[pos] != element && pos < dimension) {
			pos++;
		}

		if (pos == dimension) {
			throw HashTableException("Element does not exist in HashTable!");
		}

		this->elements[pos] = DELETED;
	}

	bool isEmpty() override {
		for (int i = 0; i < dimension; i++) {
			if (this->elements[i] != FREE && this->elements[i] != DELETED) {
				return false;
			}
		}

		return true;
	}

	bool search(const Element<KeyType, ElementType>& element) override {
		int pos = this->hash_func(element.key, dimension);
		
		while (this->elements[pos] != element && pos < dimension) {
			pos++;
		}

		return pos < dimension;
	}

	KeyType search(const ElementType& element) override {
		for (int i = 0; i < dimension; i++) {
			if (this->elements[i].element == element) {
				return this->elements[i].key;
			}
		}
		
		throw HashTableException("Element does not exist in HT!");
	}

	~OpenAdressingHashTable() {
		delete this->elements;
	}

	OpenAdressingHashTableIterator<KeyType, ElementType> begin() {
		return OpenAdressingHashTableIterator<KeyType,ElementType>(this->elements,0);
	}

	OpenAdressingHashTableIterator<KeyType, ElementType> end() {
		return OpenAdressingHashTableIterator<KeyType, ElementType>(this->elements,this->dimension);
	}
};

template<typename KeyType,typename ElementType>
class OpenAdressingHashTableIterator {
private :
	Element<KeyType, ElementType> *elements;
	int pos;
public :
	OpenAdressingHashTableIterator(OpenAdressingHashTable<KeyType, ElementType>& ht) {
		this->elements = ht.elements;
		pos = 0;
	}

	OpenAdressingHashTableIterator(Element<KeyType,ElementType>* arr,int p) : elements{ arr }, pos{ p } {};

	OpenAdressingHashTableIterator() = delete;

	bool operator!=(const OpenAdressingHashTableIterator& other) {
		return this->pos != other.pos ;
	}

	Element<KeyType, ElementType> operator*() {
			return this->elements[pos];
	}

	void operator++() {
		this->pos++;
	}
};

template<typename KeyType,typename ElementType>
class SeparateChainingHashTable : public HashTableInterface<KeyType,ElementType> {
	//separate chaining with AVL
private :
	int dimension;
	AVLTree<Element<KeyType,ElementType>> *elements;
	std::function<int(KeyType,int)> hash_func;
public :
	SeparateChainingHashTable(int dim,std::function<int(KeyType,int)> hash) : dimension{ dim } {
		this->elements = new AVLTree<Element<KeyType, ElementType>>[this->dimension];
		this->hash_func = hash;
		
	}

	//copy constructor
	SeparateChainingHashTable(const SeparateChainingHashTable& other) {
		this->hash_func = other.hash_func;
		this->elements = new AVLTree<Element<KeyType, ElementType>>[other.dimension];
		this->dimension = other.dimension;
		
		for (int i = 0; i < other.dimension; i++) {
			this->elements[i] = other.elements[i];
		}
	}

	//op= overload
	SeparateChainingHashTable<KeyType, ElementType>& operator=(const SeparateChainingHashTable& other) {
		this->~SeparateChainingHashTable();
		this->elements = new AVLTree<Element<KeyType, ElementType>>[other.dimension];
		this->dimension = other.dimension;
		
		for (int i = 0; i < other.dimension; i++){
			this->elements[i] = other.elements[i];
		}

		return *this;
	}

	void add(const Element<KeyType,ElementType>& element) override {
		int pos = this->hash_func(element.key,dimension);
	
		try {
			this->elements[pos].public_add(element);
		} catch (const BinarySearchTreeException& er) {
			throw HashTableException("Element already exists in HT!");
		}
	}

	void erase(const Element<KeyType, ElementType>& element) override {
		int pos = this->hash_func(element.key,dimension);
		
		try {
			this->elements[pos].public_erase(element);
		} catch (const BinarySearchTreeException& er) {
			throw HashTableException("Element with the given key does not exist!");
		}
	}

	bool isEmpty() override{
		for (int i = 0; i < dimension; i++) {
			if (!this->elements[i].empty()) {
				return false;
			}
		}

		return true;
	}

	bool search(const Element<KeyType, ElementType>& element) override {
		int pos = this->hash_func(element.key,dimension);
		
		try {
			this->elements[pos].public_search(element);
			return true;
		} catch (const BinarySearchTreeException& er) {
			return false;
		}
	}

	KeyType search(const ElementType& element) override {
		int i = 0;
		
		while (i < dimension) {
			try {

				KeyType key = this->elements[i].public_search(Element<KeyType,ElementType>(element))->element.key;
				return key;
			} catch (const BinarySearchTreeException& er) {
				//just continue
			}

			i++;
		}

		throw HashTableException("Element does not exist in HashTable!");
	}

	~SeparateChainingHashTable() {
		for (int i = 0; i < dimension; i++) {
			this->elements[i].~AVLTree();
		}
	}
};

void test_hash_open_adr() {
	OpenAdressingHashTable<int, int> hash(10, hash_int);
	assert(hash.isEmpty());
	hash.add(Element<int, int>(1, 3));
	hash.add(Element<int, int>(1, 2));
	
	try {
		hash.add(Element<int, int>(1, 3));
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}

	hash.add(Element<int, int>(3, 4));
	hash.add(Element<int, int>(5, 1));
	hash.add(Element<int, int>(5, 2));
	hash.add(Element<int, int>(5, 3));
	hash.add(Element<int, int>(5, 4));
	hash.add(Element<int, int>(5, 5));
	//rehash done here!
	hash.add(Element<int, int>(5, 6));
	assert(hash.search(3) == 1);
	assert(hash.search(4) == 3);
	hash.erase(Element<int, int>(1, 2));
	hash.add(Element<int, int>(1, 2));
	assert(!hash.isEmpty());
	assert(hash.search(Element<int, int>(1, 2)));
	assert(!hash.search(Element<int, int>(9, 8)));
	assert(hash.search(2) == 1);
	
	try {
		hash.search(9);
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}

	OpenAdressingHashTable<int, int> ahash{ 3,hash_int };
	ahash = hash;
	assert(!ahash.isEmpty());
	assert(ahash.search(Element<int, int>(1, 2)));
	assert(!ahash.search(Element<int, int>(9, 8)));
	
	try {
		ahash.add(Element<int, int>(1, 3));
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}

	OpenAdressingHashTable<int, int> chash{ ahash };
	assert(!chash.isEmpty());
	assert(chash.search(Element<int, int>(1, 2)));
	assert(!chash.search(Element<int, int>(9, 8)));
	
	try {
		chash.add(Element<int, int>(1, 3));
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}

	OpenAdressingHashTable<const char*, int> rhash( 8, hash_str );
	rhash.add(Element<const char*, int>("ana", 3));
	rhash.add(Element<const char*, int>("ana", 4));
	rhash.add(Element<const char*, int>("ana", 5));
	rhash.add(Element<const char*, int>("ana", 6));
	rhash.add(Element<const char*, int>("ana", 1));
	
	try {
		rhash.add(Element<const char*, int>("ana", 3));
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}
	
	assert(rhash.search(3) == "ana");
}

void test_hash_sep_chain() {
	SeparateChainingHashTable<int, int> hash(7,hash_int);
	assert(hash.isEmpty());
	hash.add(Element<int, int>(1, 1));
	hash.add(Element<int, int>(1, 3));
	hash.add(Element<int, int>(1, 2));
	hash.add(Element<int, int>(1, 0));
	hash.add(Element<int, int>(1, 5));
	hash.add(Element<int, int>(1, 6));
	
	try {
		hash.add(Element<int, int>(1, 6));
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}

	SeparateChainingHashTable<int, int> copyhash{ hash };
	assert(!copyhash.isEmpty());
	assert(copyhash.search(Element<int, int>(1, 1)));
	assert(!copyhash.search(Element<int, int>(3, 4)));
	copyhash.erase(Element<int, int>(1, 3));
	assert(!copyhash.search(Element<int, int>(1, 3)));
	assert(hash.search(Element<int, int>(1, 3)));

	SeparateChainingHashTable<int, int> operhash = hash;
	assert(!operhash.isEmpty());
	assert(operhash.search(Element<int, int>(1, 1)));
	assert(!operhash.search(Element<int, int>(3, 4)));
	operhash.erase(Element<int, int>(1, 3));
	assert(!operhash.search(Element<int, int>(1, 3)));
	assert(hash.search(Element<int, int>(1, 3)));

	assert(!hash.isEmpty());
	assert(hash.search(Element<int, int>(1, 1)));
	assert(!hash.search(Element<int, int>(3, 4)));

	hash.erase(Element<int, int>(1, 3));
	assert(!hash.search(Element<int, int>(1, 3)));
	
	try {
		hash.erase(Element<int, int>(1, 3));
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}

	assert(hash.search(1) == 1);
	
	try {
		hash.search(3);
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}
	
	SeparateChainingHashTable<const char*, int> ahash(10,hash_str);
	ahash.add(Element<const char*, int>("ana", 3));
	ahash.add(Element<const char*, int>("ana", 4));
	ahash.add(Element<const char*, int>("ana", 5));
	ahash.add(Element<const char*, int>("ana", 6));
	ahash.add(Element<const char*, int>("ana", 1));
	
	try {
		ahash.add(Element<const char*, int>("ana", 3));
		assert(false);
	} catch (const HashTableException& er) {
		assert(true);
	}

	assert(ahash.search(3) == "ana");
}
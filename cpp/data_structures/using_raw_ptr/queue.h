#pragma once
#include<string>
#include<memory>
#include<assert.h>

template<typename ElementType>
struct node {
	ElementType element;
	node<ElementType>* next;

	node(const ElementType& elem) : element{ elem }, next{ nullptr } {}
};

class QueueException {
private :
	std::string message;
public :
	QueueException(const std::string& mess) : message{ mess } {}

	friend std::ostream& operator<<(std::ostream& out, const QueueException& err) {
		out << err.message;
		return out;
	}
};

template<typename ElementType>
class Queue {
private :
	node<ElementType>* head;
	node<ElementType>* tail;
public :
	//constructor
	Queue() : head{ nullptr }, tail{ nullptr } {}

	//copy constructor
	Queue(const Queue& ot) {
		this->head = nullptr;
		this->tail = nullptr;
		node<ElementType>* other = ot.head;

		while (other != nullptr) {
			this->push(other->element);
			
			if (other->next == nullptr) {
				this->tail = this->head;
			}

			other = other->next;
		}
	}

	//op= overload
	Queue<ElementType>& operator=(Queue& ot) {
		this->~Queue();
		node<ElementType>* other = ot.head;

		while (other != nullptr) {
			this->push(other->element);
	
			if (other->next == nullptr) {
				this->tail = this->head;
			}

			other = other->next;
		}

		return *this;
	}

	//adds the element given to the queue
	void push(const ElementType& elem) {
		if (this->head == nullptr) {
			this->head = new node<ElementType>{ elem };
			this->tail = this->head;
			return;
		}

		node<ElementType>* added =  new node<ElementType>{elem};
		this->tail->next = added;
		this->tail = added;
	}

	//returns the first element of the queue and deletes it;
	//raises QueueException if the queue is empty
	ElementType pop() {
		if (this->head == nullptr) {
			throw QueueException("Queue empty!");
		}

		node<ElementType>* toDelete = this->head;
		ElementType retElement = toDelete->element;

		this->head = this->head->next;
		delete toDelete;

		return retElement;
	}

	//returns true if the queue is empty;false otherwise
	bool empty() {
		return this->head == nullptr;
	}

	~Queue() {
		node<ElementType>* toDelete = this->head;
	
		while (this->head != nullptr) {
			this->head = this->head->next;
			delete toDelete;
			toDelete = this->head;
		}
	}
};

void test_queue() {
	Queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);

	assert(q.empty() == false);

	Queue<int> q1;
	q1 = q;

	Queue<int> q2{ q };
	assert(q2.pop() == 1);
	assert(q2.pop() == 2);
	assert(q2.pop() == 3);
	assert(q2.empty());
	assert(!q.empty());

	assert(q.pop() == 1);
	assert(q.pop() == 2);
	assert(q.pop() == 3);
	
	assert(q1.empty() == false);
	assert(q1.pop() == 1);
	
	try {
		int nr = q.pop();
		assert(false);
	} catch (const QueueException& err) {
		assert(true);
	}

	assert(q.empty() == true);
}
#pragma once
#include<assert.h>
#include<functional>
#include<string>
#include "../utils/utils.h"

class HeapException {
private :
	std::string message;
public :
	HeapException(std::string msg) : message{ msg } {};

	friend std::ostream& operator<<(std::ostream& out, const HeapException& err) {
		out << err.message;
		return out;
	}
};

template<typename ElementType>
class Heap {
	/*
	* Heap implemented using an array
	* representation rules :
	* let's consider the current node's index in the array is X
	*	the node's father will be (X-1)/2
	*	the left child will be 2*X+1
	*	the right child will be 2*X+2
	*
	*/
private :
	int dimension;
	int index;

	//this function should return -1(<),0(==),1(>) depending on the compared elements
	std::function<int(ElementType, ElementType)> cmp_function;
	ElementType *elements;

	//checks if the node with the given position respects the heap property with its father;if not,they are swapped
	void _up(int pos) {
		if (!pos) {
			return;
		} else if (this->cmp_function(this->elements[pos], this->elements[int((pos - 1) / 2)]) < 0) {
			this->_swap(this->elements[pos], this->elements[int((pos - 1) / 2)]);
			this->_up(int((pos - 1) / 2));
		}
	}

	//checks if the node with the given position respects the heap property with its sons;if not, swap occurs
	void _down(int pos) {
		int left = pos * 2 + 1;
		int right = pos * 2 + 2;

		if (left >= this->index) {
			return;
		}

		int minimum = pos;

		if (this->cmp_function(this->elements[pos], this->elements[left]) > 0) {
			minimum = left;
		}

		if (right < this->index  && this->cmp_function(this->elements[minimum], this->elements[right])>0) {
			minimum = right;
		}

		if (minimum != pos) {
			this->_swap(this->elements[minimum], this->elements[pos]);
			this->_down(minimum);
		}
	}

	//swaps two elements
	void _swap(ElementType& first, ElementType& second) {
		ElementType third = first;
		first = second;
		second = third;
	}

public :
	//constructor
	Heap(std::function<int(ElementType, ElementType)> func, int dim) : cmp_function{ func }, dimension{ dim } {
		this->index = 0;
		elements = new ElementType[this->dimension];
	};

	Heap(const Heap& other) {
		this->cmp_function = other.cmp_function;
		this->index = other.index;
		this->dimension = other.dimension;
		this->elements = new ElementType[this->dimension];
		
		for (int i = 0; i < this->index; i++) {
			this->elements[i] = other.elements[i];
		}
	}

	Heap<ElementType>& operator=(const Heap& other) {
		delete this->elements;
		this->cmp_function = other.cmp_function;
		this->index = other.index;
		this->dimension = other.dimension;
		this->elements = new ElementType[this->dimension];
		
		for (int i = 0; i < this->index; i++) {
			this->elements[i] = other.elements[i];
		}
		
		return *this;
	}

	Heap() = delete;

	//adds a element to the heap
	void add(const ElementType& element) {
		if (this->index == this->dimension) {
			throw HeapException("Heap is full!");
		}

		this->elements[this->index] = element;
		this->_up(this->index);
		this->index++;
	}

	//erases the heaps root and returns a copy of it
	ElementType erase() {
		ElementType element = this->elements[0];
		this->elements[0] = this->elements[this->index - 1];
		this->_down(0);
		this->index--;

		return element;
	}

	//returns a copy of the root
	ElementType peek() {
		return this->elements[0];
	}

	//destructor
	~Heap() {
		delete this->elements;
	}
};

void test_heap() {
	Heap<int> heap{ Utils::cmp<int>,8};
	heap.add(1);
	assert(heap.peek() == 1);
	heap.add(2);
	assert(heap.peek() == 1);
	heap.add(5);
	heap.add(3);
	heap.add(8);
	heap.add(6);
	heap.add(4);
	heap.add(7);
	Heap<int> copyheap{ heap };
	Heap<int> overheap{ Utils::cmp<int>,8 };
	overheap= heap;
	try {
		heap.add(100);
		assert(false);
	} catch (const HeapException& err) {
		assert(true);
	}
	
	assert(heap.erase()==1);
	assert(copyheap.peek() == 1);
	assert(overheap.peek() == 1);
	assert(heap.peek() == 2);
	
	assert(heap.erase()==2);
	assert(copyheap.peek() == 1);
	assert(copyheap.erase() == 1);
	assert(overheap.peek() == 1);
	assert(copyheap.erase() == 2);
	assert(copyheap.erase() == 3);
	assert(heap.peek() == 3);
}

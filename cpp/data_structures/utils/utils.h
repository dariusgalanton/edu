#pragma once
#include<string>

class Exception {
protected :
	std::string message;
public :
	Exception(std::string msg);
	friend std::ostream& operator<<(std::ostream& out, const Exception& err);
};

class BinarySearchException : public Exception {
public:
	BinarySearchException(std::string msg);
};

class Utils {
public :
	//compares two given elements
	//param : first - ElementType
	//param : second - ElementType
	//return -1 if the first is smaller,0 if the params are equal ,1 otherwise
	template<typename ElementType>
	static int cmp(ElementType first, ElementType second);

	//binary search
	//param : arr - array of ElementType
	//param : element - ElementType
	//param : arrSize - int (size of array)
	template<typename ElementType>
	static int binary_search(ElementType *arr, ElementType element, int arrSize);
};

void test_cmp();
void test_bs();
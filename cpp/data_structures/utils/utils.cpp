#include"utils.h"
#include<assert.h>
#include<string>

Exception::Exception(std::string msg) : message{ msg } {};

std::ostream& operator<<(std::ostream& out, const Exception& err) {
	out << err.message;
	return out;
}

BinarySearchException::BinarySearchException(std::string msg) : Exception{ msg } {};

//compares two given elements
//param : first - ElementType
//param : second - ElementType
//return -1 if the first is smaller,0 if the params are equal ,1 otherwise
template<typename ElementType>
int Utils::cmp(ElementType first, ElementType second) {
	if (first < second) {
		return -1;
	} else if (first == second) {
		return 0;
	}

	return 1;
}

//binary search
//param : arr - array of ElementType
//param : element - ElementType
//param : arrSize - int (size of array)
template<typename ElementType>
int Utils::binary_search(ElementType *arr,ElementType element,int arrSize) {
	int begin = 0;
	int end = arrSize;

	while (begin <= end) {
		int middle = (begin + end) / 2;

		if (element < arr[middle]) {
			end = middle;
		} else if (element > arr[middle]) {
			begin = middle + 1;
		} else return middle;
	}

	throw BinarySearchException("The element does not exist in the array!");
}

void test_cmp() {
	assert(Utils::cmp<int>(1, 2) < 0);
	assert(Utils::cmp<int>(2, 2) == 0);
	assert(Utils::cmp<int>(3, 2) > 0);
	assert(Utils::cmp<std::string>("ana", "apples") < 0);
	assert(Utils::cmp<std::string>("ana", "ana") == 0);
	assert(Utils::cmp<std::string>("ana", "aaa") > 0);
}

void test_bs() {
	int vector[4];
	vector[0] = 1;
	vector[1] = 2;
	vector[2] = 3;
	vector[3] = 4;
	assert(Utils::binary_search<int>(vector, 1, 4)==0);
	assert(Utils::binary_search<int>(vector, 2, 4)==1);
	assert(Utils::binary_search<int>(vector, 3, 4)==2);
	assert(Utils::binary_search<int>(vector, 4, 4)==3);

	try {
		Utils::binary_search<int>(vector, 5, 4);
		assert(false);
	}
	catch (const BinarySearchException& err) {
		assert(true);
	}
}
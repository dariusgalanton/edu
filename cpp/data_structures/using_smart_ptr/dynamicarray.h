#pragma once
#include<memory>
#include<string>
#include<assert.h>

class DynamicArrayException {
private:
	std::string message;
public:
	DynamicArrayException(const std::string& mess) : message{ mess } {}

	friend std::ostream& operator<<(std::ostream& out, const DynamicArrayException& err) {
		out << err.message;
		return out;
	}
};

template<typename ElementType>
class DynamicArrayIterator;

template<typename ElementType>
class DynamicArray {
private :
	int capacity;
	int length;
	std::shared_ptr<ElementType> list;

	//reallocates the array with a bigger capacity
	void reallocate() {
		this->capacity = this->capacity * 2;
		std::shared_ptr<ElementType> newList{ new ElementType[capacity], [](ElementType* ptr) {delete[] ptr; } };

		for (int i = 0; i < this->length; i++) {
			newList.get()[i] = this->list.get()[i];
		}

		this->list = newList;
	}

public :
	//default constructor
	DynamicArray() : capacity{ 10 }, length{ 0 }, list{ nullptr } {
		this->list.reset(new ElementType[this->capacity], [](ElementType* ptr) {delete[] ptr; });
	}
	
	//copy constructor
	DynamicArray(const DynamicArray& ot) {
		this->capacity = ot.capacity;
		this->length = ot.length;
		this->list.reset(new ElementType[this->capacity]);
		
		for (int i = 0; i < ot.length; i++) {
			this->list.get()[i] = ot.list.get()[i];
		}
	}

	//op= overloading
	DynamicArray<ElementType>& operator=( DynamicArray ot) {
		this->capacity = ot.capacity;
		this->length = ot.length;
		this->list.reset(new ElementType[this->capacity]);

		for (int i = 0; i < ot.length; i++) {
			this->list.get()[i] = ot.list.get()[i];
		}

		return *this;
	}

	//returns the size of the array
	//return : length - int
	int size() {
		return length;
	}

	//adds the given element to the array and performs reallocate if neccesary
	//param : element - ElementType
	void push_back(const ElementType& element) {
		if (this->capacity == this->length) {
			this->reallocate();
		}

		this->list.get()[length] = element;
		this->length += 1;
	}

	//erases the element from a given position
	//throws DynamicArrayException if the position does not exist in the array
	//param : pos - int
	void erase(int pos) {
		if (pos >= this->length || pos<0) {
			throw DynamicArrayException("Out of bounds!");
		}

		for (int i = pos; i < this->length - 1; i++) {
			this->list.get()[i] = this->list.get()[i + 1];
		}

		this->length -= 1;
	}

	//operator[] overloading
	//param : pos - int
	//returns a reference to the element of the given position
	ElementType& operator[](int pos) {
		return this->list.get()[pos];
	}

	//operator+ overloading;
	//the arrays can concatenate as in Python
	//param : ot - DynamicArray<ElementType>
	DynamicArray<ElementType>& operator+(DynamicArray ot) {
		for (auto element : ot)
			this->push_back(element);
		return *this;
	}

	//inserts a given element at a given index
	//throws DynamicArrayException if the index is out of range
	//param : element - ElementType
	//param : pos - int
	void insert(const ElementType& element, int pos) {
		if (pos >= this->length || pos < 0) {
			throw DynamicArrayException("Out of bounds!");
		}

		if (this->capacity == this->length) {
			this->reallocate();
		}

		this->length += 1;

		for (int i = this->length - 1; i > pos; i--) {
			this->list.get()[i] = this->list.get()[i - 1];
		}

		this->list.get()[pos] = element;
	}

	//erases the last element of the array and returns a copy of it
	//throws DynamicArrayException if the array is empty
	//return : ElementType
	ElementType pop() {
		if (!this->length) {
			throw DynamicArrayException("Array empty!");
		}

		this->length -= 1;
		
		return this->list.get()[this->length];
	}

	//begin function for iterator
	DynamicArrayIterator<ElementType> begin() {
		return DynamicArrayIterator<ElementType>(this->list, 0);
	}

	//end function for iterator
	DynamicArrayIterator<ElementType> end() {
		return DynamicArrayIterator<ElementType>(this->list, length);
	}
};

template<typename ElementType>
class DynamicArrayIterator {
private :
	std::weak_ptr<ElementType> list;
	int index;
public :
	DynamicArrayIterator(std::shared_ptr<ElementType> ptr, int ind) : index{ ind } ,list{ptr} {}

	bool operator!=(const DynamicArrayIterator<ElementType>& ot) {
		return this->index != ot.index;
	}

	void operator++() {
		this->index += 1;
	}

	ElementType operator*() {
		if (std::shared_ptr<ElementType> sptr = this->list.lock()) {
			return sptr.get()[this->index];
		}

		return NULL;
	}
};

void test_array() {
	DynamicArray<int> arr;
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);

	assert(arr.size() == 3);
	assert(arr[0] == 1);
	assert(arr[1] == 2);
	assert(arr[2] == 3);

	DynamicArray<int> otarr{ arr };
	assert(otarr.size() == 3);
	assert(otarr[0] == 1);
	assert(otarr[1] == 2);
	assert(otarr[2] == 3);
	otarr[0] = 4;
	assert(arr[0] == 1);

	DynamicArray<int> op;
	op = arr;
	assert(op[0] == 1);
	assert(op[1] == 2);
	assert(op[2] == 3);

	op = op + arr;
	assert(op[0] == 1);
	assert(op[1] == 2);
	assert(op[2] == 3);
	assert(op[3] == 1);
	assert(op[4] == 2);
	assert(op[5] == 3);

	arr.erase(1);

	assert(arr.size() == 2);
	assert(arr[1] == 3);

	arr[0] = 5;

	assert(arr[0] == 5);

	//testing the realloc
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);

	assert(arr.size() == 17);

	arr.insert(100, 3);

	assert(arr[2] == 1);
	assert(arr[3] == 100);
	assert(arr[4] == 2);
	
	assert(arr.pop() == 3);

	DynamicArray<int> newArr;

	try {
		newArr.pop();
		assert(false);
	} catch (const DynamicArrayException& err) {
		assert(true);
	}

	try {
		newArr.insert(1, 1000);
		assert(false);
	} catch (const DynamicArrayException& err) {
		assert(true);
	}

	try {
		newArr.erase(10);
		assert(false);
	} catch (const DynamicArrayException& err) {
		assert(true);
	}
}

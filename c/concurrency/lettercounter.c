/*
 *	This program receives files as arguments and launches a thread for each one of them
 *	to count the number of letters appearing in the file in a struct.
 *	Implemented under Ubuntu(Linux distribution).It will not work on Windows because of the usage of pthread library.
 */
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>

typedef struct {
	char letter;
	int freq;
} tuple;

typedef struct {
	tuple tup[52];
} alphabet;

alphabet alph;
pthread_mutex_t mutexes[52];

//initializes the given alphabet
//param : alph - *alphabet
//return : void
void initialize_alphabet(alphabet* alph) {
	int i, pos = 0;

	for (i = 'A'; i <= 'Z'; i++, pos++) {
		alph->tup[pos].letter = i;
		alph->tup[pos].freq = 0;
	}

	for (i = 'a'; i <= 'z'; i++, pos++) {
		alph->tup[pos].letter = i;
		alph->tup[pos].freq = 0;
	}
}

//prints the given alphabet
//param : alph -*alphabet
//return : void
void print_alphabet(alphabet* alph) {
	int i;

	for (i = 0; i < 52; i++) {
		if (alph->tup[i].freq) {
			printf("%c -> %d\n", alph->tup[i].letter, alph->tup[i].freq);
		}
	}
}

//thread function
//receives a filename ,opens the file and increments the letter counter
//param : filename - char*
//return : null
void* count(char* fileName) {
	FILE* fileIn = fopen(fileName, "r");

	if (fileIn == NULL) {
		//if we couldn't open the file we will simply ignore it
		return NULL;
	}

	char *buffer = (char*)malloc(128 * sizeof(char));

	while (fgets(buffer, 128, fileIn)) {
		int i;

		for (i = 0; i < strlen(buffer); i++) {
			if (buffer[i] >= 'A' && buffer[i] <= 'Z') {
				pthread_mutex_lock(&mutexes[buffer[i] - 'A']);
				alph.tup[buffer[i] - 'A'].freq++;
				pthread_mutex_unlock(&mutexes[buffer[i] - 'A']);
			} else if (buffer[i] >= 'a' && buffer[i] <= 'z') {
				pthread_mutex_lock(&mutexes[buffer[i] - 'a' + 26]);
				alph.tup[buffer[i] - 'a' + 26].freq++;
				pthread_mutex_unlock(&mutexes[buffer[i] - 'a' + 26]);
			}
		}
	}

	free(buffer);
	fclose(fileIn);

	return NULL;
}

int main(int argc, char **argv) {
	int i;
	pthread_t threads[argc];
	initialize_alphabet(&alph);

	//mutex initialization
	for (i = 0; i < 52; i++) {
		pthread_mutex_init(&mutexes[i], NULL);
	}

	//threads start
	for (i = 1; argv[i]; i++) {
		pthread_create(&threads[i], NULL, count, argv[i]);
	}

	//waiting for threads to finish
	for (i = 1; argv[i]; i++) {
		pthread_join(threads[i], NULL);
	}

	print_alphabet(&alph);

	//mutex destroy
	for (i = 0; i < 52; i++) {
		pthread_mutex_destroy(&mutexes[i]);
	}

	return 0;
}

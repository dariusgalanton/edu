/*
 * Program that receives a list of numbers from the command line and prints the steps required
 * to reach 1 in the Collatz Conjecture.It launches a POSIX thread separately for each number.
 *
 * Collatz Conjecture :
 * 	Take any positive integer n.If n is even,divide n by 2 to get n/2.If n is odd,multiply n by 3
 * 	and add 1 to get 3n+1.Repeat the process indefinitely.The conjecture states that no matter the
 * 	number,you will always reach 1 eventually.
 */

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

pthread_mutex_t mutex;

/*
 * thread function
 *
 * receives a number ,applies the Collatz Conjecture and prints the number of steps required to reach 1.
 */
void* collatz(int* number) {
	int steps = 0, nb = *number;

	while (nb != 1) {
		if (nb % 2 == 0) {
			nb = nb / 2;
		} else {
			nb = nb * 3 + 1;
		}

		steps += 1;
	}

	pthread_mutex_lock(&mutex);
	printf("For %d to reach 1 in the Collatz Conjecture, there are %d steps to be taken.\n", *number, steps);
	pthread_mutex_unlock(&mutex);
	free(number);

	return NULL;
}

int main(int argc, char **argv) {
	int i;
	pthread_t threads[argc];

	//mutex init
	pthread_mutex_init(&mutex, NULL);

	//threads start
	for (i = 1; argv[i]; i++) {
		int* number = (int*)malloc(sizeof(int));
		*number = atoi(argv[i]);
		pthread_create(&threads[i], NULL, collatz, number);
	}

	//waiting for threads to finish
	for (i = 1; argv[i]; i++) {
		pthread_join(threads[i], NULL);
	}

	//mutex destroy
	pthread_mutex_destroy(&mutex);

	return 1;
}


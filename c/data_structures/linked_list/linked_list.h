#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#define ElementType int
#define DELETE_FAILED 1
#define INDEX_OUT_OF_BOUNDS 2
#define LIST_EMPTY 3
#define OPERATION_SUCCESSFUL 4

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * Concurrent Linked List
 */

typedef struct node_st {
	ElementType element;
	struct node_st* next;
} node;

typedef struct {
	pthread_mutex_t mutex;
	node* head;
	int size;
} List;

/*
 * List initialization
 */
void init(List* list);

/*
 * Adds the given element to the front of the list
 */
void push_front(List* list, ElementType element);

/*
 * Adds the given element to the end of the list
 */
void push_back(List* list, ElementType element);

/*
 * Inserts the given element at the given index of the list
 * Return codes : INDEX_OUT_OF_BOUNDS - if the given index is out of the bounds of the list
 * 		OPERATION_SUCCESSFUL - if the element was sucessfully inserted
 */
int insert(List* list, ElementType element, int index);

/*
 * Deletes the first element of the list and returns a copy of it
 * Return codes : LIST_EMPTY - if the list is empty
 */
ElementType pop(List* list);

/*
 * Deletes a given element from the list
 * Return codes : DELETE_FAILED - if the delete failed(the element is not in the list)
 * 		OPERATION_SUCCESSFUL - if the element was successfully removed
 */
int delete(List* list, ElementType element);

/*
 * Destroys the given list
 */
void destroy(List* list);

/*
 * Prints the given list to stdout
 */
void printList(List* list);

#endif

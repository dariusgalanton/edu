#include "linked_list.h"

void init(List* list) {
	pthread_mutex_init(&(list->mutex), NULL);
	list->head = NULL;
	list->size = 0;
}

void destroy(List* list) {
	pthread_mutex_destroy(&(list->mutex));
	node* head = list->head;

	while (head != NULL) {
		node* toDelete = head;
		head = head->next;
		free(toDelete);
	}
}

void push_front(List* list, ElementType element) {
	node* newNode = (node*)malloc(sizeof(node));
	newNode->element = element;
	newNode->next = list->head;

	pthread_mutex_lock(&(list->mutex));
	list->head = newNode;
	list->size += 1;
	pthread_mutex_unlock(&(list->mutex));
}

void push_back(List* list, ElementType element) {
	node* currentNode = list->head;
	node* newNode = (node*)malloc(sizeof(node));
	newNode->element = element;
	newNode->next = NULL;

	if (list->head == NULL) {
		pthread_mutex_lock(&(list->mutex));
		list->head = newNode;
		list->size = 1;
		pthread_mutex_unlock(&(list->mutex));

		return;
	}

	while (currentNode->next != NULL) {
		currentNode = currentNode->next;
	}

	pthread_mutex_lock(&(list->mutex));
	currentNode->next = newNode;
	list->size += 1;
	pthread_mutex_unlock(&(list->mutex));
}

void printList(List* list) {
	node* head = list->head;

	while (head != NULL) {
		printf("%d ", head->element);
		head = head->next;
	}

	printf("\n");
}

ElementType pop(List* list) {
	if (list->size == 0) {
		return LIST_EMPTY;
	}

	pthread_mutex_lock(&(list->mutex));
	ElementType element = list->head->element;
	node* toDelete = list->head;
	list->head = list->head->next;
	free(toDelete);
	list->size -= 1;
	pthread_mutex_unlock(&(list->mutex));

	return element;
}

int delete(List* list, ElementType element) {
	if (list->size == 0) {
		return DELETE_FAILED;
	} else if (list->head->element == element) {
		pop(list);

		return OPERATION_SUCCESSFUL;
	}

	pthread_mutex_lock(&(list->mutex));
	node* currentNode = list->head;

	while (currentNode->next != NULL && currentNode->next->element != element) {
		currentNode = currentNode->next;
	}

	if (currentNode->next == NULL) {
		pthread_mutex_unlock(&(list->mutex));

		return DELETE_FAILED;
	} else {
		node* toDelete = currentNode->next;
		currentNode->next = toDelete->next;
		free(toDelete);
		list->size -= 1;
		pthread_mutex_unlock(&(list->mutex));

		return OPERATION_SUCCESSFUL;
	}
}

int insert(List* list, ElementType element, int index) {
	if (index >= list->size || index < 0) {
		return INDEX_OUT_OF_BOUNDS;
	} else if (index == 0) {
		push_front(list, element);

		return OPERATION_SUCCESSFUL;
	}

	node* newNode = (node*)malloc(sizeof(node));
	newNode->element = element;
	pthread_mutex_lock(&(list->mutex));
	node* currentNode = list->head;
	int i = 0;

	while (currentNode->next != NULL && i < index - 1) {
		currentNode = currentNode->next;
		i += 1;
	}

	newNode->next = currentNode->next;
	currentNode->next = newNode;
	list->size += 1;
	pthread_mutex_unlock(&(list->mutex));

	return OPERATION_SUCCESSFUL;
}

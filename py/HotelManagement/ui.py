import domain
import ctrl
import validator
import repo
import settings

class UserInterface(object) :
    def __init__(self,controller,valid):
        self.ctrl=controller
        self.valid=valid

    def menu(self):
        '''
        prints the menu
        :return: none
        '''
        print 'help - lists the help menu'
        print 'exit - exits the app'
        print 'add - adds a new booking'
        print 'erase - erases the booking'
        print 'sort - lists the bookings sorted'
        print 'filter - filters the bookings'
        print 'print - lists all the bookings'

    def add(self):
        '''
        reads input and adds a new booking
        :return: none
        '''
        while True :
            lst=raw_input('Introduce client (name,pnc,age) :')
            name, pnc, age=lst.split()
            try :
                self.valid.validate(int,int(pnc),int(age))
                self.valid.validate(str,name)
                break
            except validator.ValidatorException as err :
                print err
            except ValueError :
                print 'Args not valid!'
        while True :
            lst=raw_input('Introduce room details(number,tariff,beds) : ')
            number, tariff, beds=lst.split()
            try :
                self.valid.validate(int,int(number),int(tariff),int(beds))
                break
            except validator.ValidatorException as err :
                print err
            except ValueError :
                print 'Args not valid!'
        while True :
            lst=raw_input('Introduce checkin and checkout : ')
            checkin, checkout=lst.split()
            try :
                self.valid.isValidDate(checkin)
                self.valid.isValidDate(checkout)
                break
            except validator.ValidatorException as err :
                print err
        aClient=domain.Client(name,int(pnc),int(age))
        aRoom=domain.Room(number,tariff,beds)
        aBooking=domain.Booking(aClient,aRoom,checkin,checkout)
        self.ctrl.add(aBooking)


    def erase(self):
        '''
        reads from input and deletes a booking
        :return: none
        '''
        while True :
            id_=raw_input('Introduce id : ')
            try :
                id_=int(id_)
                if id > 0 :
                    break
                else :
                    raise ValueError
            except ValueError :
                print 'ID not valid!'
        try :
            self.ctrl.erase(id_)
        except repo.RepositoryException as err :
            print err

    def sort(self):
        print "1-Sort by client's name"
        print "2-Sort by client's pnc"
        print "3-Sort by client's age"
        print "4-Sort by room's number"
        print "5-Sort by room's tariff"
        print "6-Sort by room's beds"
        print "7-Sort by checkin"
        print "8-Sort by checkout"
        print 'back - go back to main menu'
        while True :
            comm=raw_input('Introduce command : ')
            lst = []
            if comm == 'back' :
                break
            if comm == '1' :
                lst = self.ctrl.sort(l_key=lambda x:x.client.name)
            if comm == '2' :
                lst = self.ctrl.sort(l_key=lambda x:x.client.pnc)
            if comm == '3' :
                lst = self.ctrl.sort(l_key=lambda x:x.client.age)
            if comm == '4' :
                lst =self.ctrl.sort(l_key=lambda x:x.room.number)
            if comm == '5' :
                lst = self.ctrl.sort(l_key=lambda x:x.room.tariff)
            if comm == '6' :
                lst = self.ctrl.sort(l_key=lambda x:x.room.beds)
            if comm == '7' :
                lst =self.ctrl.sort(l_key=lambda x:x.checkin)
            if comm == '8' :
                lst =self.ctrl.sort(l_key=lambda x:x.checkout)
            if lst :
                for elem in lst :
                    #TO DO : refactorizing this part of code;build a function that prints a list of bookings
                    print '{} {} {},{} {} {},{} {}'.format(
                                                    elem.client.name,
                                                    elem.client.pnc,
                                                    elem.client.age,
                                                    elem.room.number,
                                                    elem.room.tariff,
                                                    elem.room.beds,
                                                    elem.checkin,
                                                    elem.checkout)


    def filter(self):
        print "1 - filter by client's name"
        print "2 - filter by room's number"
        print "3 - filter by checkin"
        print 'back - go back to main menu'
        while True :
            comm = raw_input('Introduce command : ')

            lst = []
            if comm == 'back':
                break
            if comm == '1' :
                keyword = raw_input('Introduce keyword : ')
                lst = self.ctrl.filter(lambda x:x.client.name,keyword)
            if comm == '2' :
                keyword = raw_input('Introduce keyword : ')
                lst = self.ctrl.filter(lambda x: x.room.number, keyword)
            if comm == '3' :
                keyword = raw_input('Introduce keyword : ')
                lst = self.ctrl.filter(lambda x: x.checkin, keyword)
            if lst :
                for elem in lst :
                    #TO DO : refactorizing this part of code;build a function that prints a list of bookings
                    print '{} {} {},{} {} {},{} {}'.format(
                                                    elem.client.name,
                                                    elem.client.pnc,
                                                    elem.client.age,
                                                    elem.room.number,
                                                    elem.room.tariff,
                                                    elem.room.beds,
                                                    elem.checkin,
                                                    elem.checkout)


    def printBookings(self):
        lst=[ ]
        for key in self.ctrl.repo.bookdict :
            booking=self.ctrl.repo.bookdict[key]
            print '{} : {} {} {},{} {} {},{} {}'.format(key,
                                                   booking.client.name,
                                                   booking.client.pnc,
                                                   booking.client.age,
                                                   booking.room.number,
                                                   booking.room.tariff,
                                                   booking.room.beds,
                                                   booking.checkin,
                                                   booking.checkout)


    def run(self):
        '''
        app entry
        :return: none
        '''
        self.menu()
        command=''
        while True :
            command=raw_input('Give a command : ')
            if command not in ['help','exit','add','erase','sort','filter','print'] :
                continue
            if command == 'help' :
                self.menu()
            if command == 'exit' :
                break
            if command == 'add' :
                self.add()
            if command == 'erase' :
                self.erase()
            if command == 'sort' :
                self.sort()
            if command == 'filter' :
                self.filter()
            if command == 'print' :
                self.printBookings()


#app entry
def app() :
    try :
        if settings.STORAGE_TYPE=='json' :
            rep = repo.JSONRepository(settings.STORAGE_PATH)
        elif settings.STORAGE_TYPE=='sqlite' :
            rep = repo.SQLRepository(settings.STORAGE_PATH)
        else :
            print 'Settings file corrupted!The app will now close'
            return
    except repo.RepositoryException as err:
        print err
    ctr = ctrl.Controller(rep)
    valid = validator.DataValidator()
    interface=UserInterface(ctr,valid)
    interface.run()

app()


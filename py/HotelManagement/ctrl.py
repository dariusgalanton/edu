import repo
class Controller(object) :
    def __init__(self,rep):
        '''
        :param rep: *Repository
        '''
        self.repo = rep

    def add(self,booking):
        '''
        invokes the add from repo,so that i don't have to write something.else.other.etc..
        :param booking: Booking
        :return: none
        '''
        self.repo.add(booking)

    def erase(self,id_):
        '''
        invokes the erase from repo,so that i don't have to write something.else.other.etc..
        :param id_: int
        :return: none
        '''
        self.repo.erase(id_)

    def sort(self,l_key):
        '''
        returns a list of bookings sorted by the given key
        :param l_key: lambda function
        :return: list
        '''
        lst=[self.repo.bookdict[key] for key in self.repo.bookdict]
        lst.sort(key=l_key)
        return lst

    def filter(self,filter_key,keyword):
        '''
        returns a list of bookings filtered by the keyword
        :param filter_key: lambda function
        :param keyword: string
        :return: list
        '''
        lst = [self.repo.bookdict[id_] for id_ in self.repo.bookdict]
        ret_lst = [element for element in lst if filter_key(element)==keyword]
        return ret_lst

def test_controller() :
    rep = repo.JSONRepository('tests.json')
    ctrl=Controller(rep)
    lst = ctrl.sort(l_key=lambda x:x.client.name)
    assert lst[0].client.name == 'John'
    assert lst[1].client.name == 'Tester'

    lst = ctrl.filter(lambda x:x.client.name,'John')
    assert len(lst) == 1
    assert lst[0].client.name == 'John'

    lst = ctrl.filter(lambda x:x.room.number,3)
    assert len(lst) == 2
    assert lst[0].room.tariff == 80
    assert lst[1].client.name == 'John'
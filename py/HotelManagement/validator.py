class ValidatorException(Exception) :
    pass

class DataValidator(object) :
    def isValidDate(self,date):
        '''
        returns true if the date given respects the format;
        raises ValidatorException otherwise
        :param date: string
        :return: boolean
        '''
        if len(date) != 10 :
            raise ValidatorException('Invalid date!')
        if date[2] != '/' or date[5] !='/' :
            raise ValidatorException('Invalid date!')
        for pos in xrange(len(date)) :
            if pos != 2 and pos != 5 and date[pos] not in '1234567890' :
                raise ValidatorException('Invalid date!')
        return True

    def validate(self,obj,*args):
        '''
        verifies if all the arguments given are an istance of obj;
        returns true if they all are;
        raises ValidatorException otherwise
        :param obj: Object
        :param args: all kind of datatypes
        :return: boolean
        '''
        for arg in args :
            if not isinstance(arg,obj) :
                raise ValidatorException('The arguments are not valid!')
        return True

def test_data_validator() :
    valid=DataValidator()
    assert valid.isValidDate('01/02/3000')
    try :
        valid.isValidDate('1/12/3001')
        assert False
    except ValidatorException :
        assert True
    try :
        valid.isValidDate('01-02-2005')
        assert False
    except ValidatorException :
        assert True
    try :
        valid.isValidDate('01/02/200p')
        assert False
    except ValidatorException :
        assert True
    #validate(obj,args) test
    assert valid.validate(int,1,2,3,4,5)
    assert valid.validate(str,'ana','has','apples')
    assert valid.validate(list,[1,2],['ana','has'])
    try :
        valid.validate(int,'a','n','a')
        assert False
    except ValidatorException :
        assert True

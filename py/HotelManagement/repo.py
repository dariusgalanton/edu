import domain
import json
import os
import sqlite3

class RepositoryException(Exception) :
    pass



class InMemoryRepository(object) :
    def __init__(self):
        self.bookdict = {}

    def ensure_id(self):
        '''
        ensures the id for the next element
        :return: int
        '''
        maximum=0
        for id_ in self.bookdict :
            if id_ > maximum :
                maximum = id_
        return maximum+1

    def add(self,booking):
        '''
        adds the given booking to the dict
        :param booking: Booking
        :return: none
        '''
        self.bookdict[self.ensure_id()]=booking

    def erase(self,id_):
        '''
        erases the booking from a given id
        :param id_: int
        :return: none
        '''
        if id_ in self.bookdict :
            del self.bookdict[id_]
        else :
            raise RepositoryException('The booking with the given ID does not exist!')



class JSONRepository(InMemoryRepository) :
    def __init__(self,fileName):
        '''
        :param fileName: string
        '''
        super(InMemoryRepository,self).__init__()
        self.bookdict = {}
        self.fileName=fileName
        self._load()

    def loadDict_from_json(self,jsonfile):
        '''
        returns a dict from the given json file
        :param jsonfile: string
        :return: dic - dictionary
        '''
        with open(os.path.abspath(os.path.expanduser(jsonfile)), 'r') as jsonRead:
            dic = json.load(jsonRead)
        return dic

    def _load(self):
        '''
        loads into memory the dictionary of bookings from json
        :return: none
        raises RepositoryException if the json file couldn't be opened
        '''
        try :
            dic = self.loadDict_from_json(self.fileName)
            for key in dic :
                name,pnc,age = dic[key][0]
                number,tariff,beds = dic[key][1]
                checkin=dic[key][2]
                checkout=dic[key][3]
                aClient = domain.Client(name,pnc,age)
                aRoom = domain.Room(number,tariff,beds)
                aBooking = domain.Booking(aClient,aRoom,checkin,checkout)
                self.bookdict[int(key)] = aBooking
        except IOError :
            raise RepositoryException("JSON file {} couldn't be opened for reading!".format(self.fileName))

    def _save(self):
        '''
        saves the bookings dictionary to the json file attached to the repository
        :return: none
        raises RepositoryException if the json file couldn't be opened
        '''
        try :
            with open(os.path.abspath(os.path.expanduser(self.fileName)),'w') as jsonWrite :
                dic = {}
                for key in self.bookdict :
                    client=[self.bookdict[key].client.name,self.bookdict[key].client.pnc,self.bookdict[key].client.age]
                    room=[self.bookdict[key].room.number,self.bookdict[key].room.tariff,self.bookdict[key].room.beds]
                    dic[key]=[client,room,self.bookdict[key].checkin,self.bookdict[key].checkout]
                json.dump(dic,jsonWrite,indent=4)
        except IOError :
            raise RepositoryException("JSON file {} couldn't be opened for writing!".format(self.fileName))

    def add(self, booking):
        '''
        adds the given booking to the dict
        :param booking: Booking
        :return: none
        '''
        self.bookdict[self.ensure_id()] = booking
        self._save()

    def erase(self,id_):
        '''
        erases the booking from a given id
        :param id_: int
        :return: none
        '''
        if id_ in self.bookdict :
            del self.bookdict[id_]
            self._save()
        else :
            raise RepositoryException('The booking with the given ID does not exist!')



class SQLRepository(InMemoryRepository) :
    def __init__(self,db):
        '''
        :param db: sql database
        '''
        super(InMemoryRepository,self).__init__()
        self.bookdict={}
        self.db=db
        self._load()

    def _load(self):
        '''
        loads the bookings from the database
        :return: none
        raises RepositoryException if the database couldn't be opened
        '''
        try :
            read = sqlite3.connect(os.path.abspath(os.path.expanduser(self.db)))
            run = read.cursor()
            run.execute('SELECT id,name,pnc,age,roomnb,tariff,beds,checkin,checkout FROM bookings')
            for line in run :
                id,name,pnc,age,number,tariff,beds,checkin,checkout=line
                aClient=domain.Client(name,pnc,age)
                aRoom=domain.Room(number,tariff,beds)
                aBooking=domain.Booking(aClient,aRoom,checkin,checkout)
                self.bookdict[id]=aBooking
            read.close()
        except sqlite3.DatabaseError:
            raise RepositoryException("The SQL database couldn't be opened for reading!")

    def _save(self):
        '''
        saves the bookings in the database
        :return: none
        raises RepositoryException if the database couldn't be opened
        '''
        try:
            write = sqlite3.connect(os.path.abspath(os.path.expanduser(self.db)))
            writer = write.cursor()
            writer.execute('DROP TABLE bookings ')
            writer.execute('CREATE TABLE bookings (id INTEGER PRIMARY KEY UNIQUE,name TEXT,pnc INTEGER,age INTEGER,roomnb INTEGER,tariff INTEGER,beds INTEGER,checkin TEXT,checkout TEXT)')
            for key in self.bookdict:
                writer.execute('INSERT OR REPLACE INTO bookings VALUES(?,?,?,?,?,?,?,?,?)',
                               (key,
                                self.bookdict[key].client.name,
                                self.bookdict[key].client.pnc,
                                self.bookdict[key].client.age,
                                self.bookdict[key].room.number,
                                self.bookdict[key].room.tariff,
                                self.bookdict[key].room.beds,
                                self.bookdict[key].checkin,
                                self.bookdict[key].checkout
                                ))
            write.commit()
            write.close()
        except sqlite3.DatabaseError:
            raise RepositoryException("The SQL database couldn't be opened for writing!")

    def add(self,booking):
        '''
        adds the booking to the bookdict
        :param booking: Booking
        :return: none
        '''
        self.bookdict[self.ensure_id()]=booking
        self._save()

    def erase(self,id_):
        '''
        erases the booking with the given id
        :param id_: int
        :return: none
        '''
        if id_ in self.bookdict :
            del self.bookdict[id_]
            self._save()
        else :
            raise RepositoryException('The booking with the given ID does not exist!')


def test_json_repository() :
    rep=JSONRepository('tests.json')

    assert rep.bookdict[1].client.name == 'Tester'
    assert rep.bookdict[2].client.age == 30

    client=domain.Client('Gerrard',123456,30)
    room=domain.Room(3,80,1)
    book=domain.Booking(client,room,'14/05/2017','21/05/2017')

    rep.add(book)

    assert rep.bookdict[3].client.name == 'Gerrard'

    rep.erase(3)

    try :
        rep.erase(3)
        assert False
    except RepositoryException :
        assert True

    try :
        rep=JSONRepository('notexistent.json')
        assert False
    except RepositoryException :
        assert True



def test_sql_repository() :

    rep = SQLRepository('test.db')

    assert rep.bookdict[1].client.name == 'Tester'
    assert rep.bookdict[1].client.pnc == 123456
    assert rep.bookdict[1].client.age == 30

    client = domain.Client('John', 123456, 30)
    room = domain.Room(3, 80, 1)
    book = domain.Booking(client, room,'22/03/2017','24/03/2017')

    rep.add(book)

    assert rep.bookdict[2].client.name == 'John'

    rep.erase(2)

    try:
        rep.erase(2)
        assert False
    except RepositoryException:
        assert True

    try:
        rep = SQLRepository('nonexistent.db')
        assert False
    except RepositoryException:
        assert True






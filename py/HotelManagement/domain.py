class Client(object) :
    def __init__(self,name,pnc,age):
        '''
        :param name: string
        :param pnc: int
        :param age: int
        '''
        self.name=name
        self.pnc=pnc
        self.age=age

    def __eq__(self, other):
        return self.pnc==other.pnc

    def __ne__(self, other):
        return not self==other

class Room(object) :
    def __init__(self,number,tariff,beds):
        '''
        :param number: int
        :param tariff: int
        :param beds: int
        '''
        self.number=number
        self.tariff=tariff
        self.beds=beds

    def __eq__(self, other):
        return self.number==other.number

    def __ne__(self, other):
        return not self==other


class Booking(object) :
    def __init__(self,client,room,checkin,checkout):
        '''
        :param client:  Client
        :param room: Room
        :param checkin : string
        :param checkout : string
        '''
        self.client=client
        self.room=room
        self.checkin=checkin
        self.checkout=checkout

    def __eq__(self, other):
        return self.client==other.client and self.room==other.room and self.checkin==other.checkin and self.checkout==other.checkout

    def __ne__(self, other):
        return not self==other

def test_client() :
    first=Client('John',123,18)
    second=Client('Richard',312,25)
    third=Client('Gerald',123,55)
    assert first != second
    assert first == third

def test_room() :
    first=Room(1,30,2)
    second=Room(2,45,3)
    third=Room(1,44,3)
    assert first==third
    assert first!=second
    assert second!=third

def test_booking():
    client1 = Client('John', 123, 18)
    client3 = Client('Gerald', 123, 55)
    room1 = Room(1, 30, 2)
    room2 = Room(2, 45, 3)
    room3 = Room(1, 44, 3)

    book1=Booking(client1,room1,'24/04/2017','30/04/2017')
    book2=Booking(client3,room2,'23/03/2017','25/03/2017')
    book3=Booking(client1,room3,'24/04/2017','30/04/2017')

    assert book1==book3
    assert book1!=book2
;asm 8086

;Program that receives a list of numbers , converts them to base 16 and prints them
assume cs:code,ds:data

data segment
	list dw 12,16,10,33
	len equ $-list	;len will be a constant representing the number of numbers to convert
	remainders db '0123456789ABCDEF'
data ends

code segment
conv16 proc
	;converts the number from ax register and prints it 
	push bx
	push cx
	push dx
	
	mov bx,16	;base to convert
	mov cx,0	;cx = number of remainders
	while :
		cmp ax,0
		je endwhile
		mov dx,0
		div bx
		push dx ;pushing the remainders on stack to extract them in reverse order
		inc cx
		jmp while
	endwhile :
	
	cmp cx,0
	je endprint
	mov bx,offset remainders
	print :
		pop ax
		xlat remainders
		mov dl,al
		mov al,0
		mov ah,02h	;dos interrupt to write to stdout
		int 21h
	loop print 	
	endprint :
	
	mov dl,' '
	mov al,0	;write a space to stdout
	mov ah,02h
	int 21h
	
	pop dx
	pop cx
	pop bx
	ret
conv16 endp

start :
	mov ax,data
	mov ds,ax
	
	mov cx,len
	mov di,0
	for_each :
		mov ax,list[di]
		call conv16
		add di,2
		dec cx
	loop for_each
	
	mov ax,4c00h
	int 21h
code ends
end start
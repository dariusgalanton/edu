;asm 8086

;scanf implementation

assume cs:code,ds:data

data segment public
	TEN dw 10
	max_buffer db  5
	len_buffer db ?
	buffer db 5 dup(?)
data ends

code segment public
public scanf
mov ax,data
mov ds,ax
scanf proc
	;reads from stdin , converts for the given format from bx
	;if it's a number , it will be stored in ax
	;if it's a string , its offset will be stored in ax
	push cx
	push dx

	mov ah,0Ah	;dos interrupt for keyboard reading
	mov dx,offset max_buffer
	int 21h
	mov di,0
	mov cl,len_buffer
	mov ch,0
	mov ax,0
	cmp bx,'d' ;if the given format is decimal
	je decimal
	cmp bx,'s' ;if the given format is a string
	je string
	
	decimal :
		mov bl,buffer[di]
		sub bl,48
		mov bh,0
		cmp ax,0
		mul TEN
		add ax,bx
		inc di
	loop decimal
	jmp return	
	
	string :
		;making the string end in 0(ASCIIZ)
		mov al,len_buffer
		xor ah,ah
		mov si,ax
		mov buffer[si],0
		mov ax,offset buffer
		
	return :
		pop dx
		pop cx
		ret
scanf endp
code ends
end